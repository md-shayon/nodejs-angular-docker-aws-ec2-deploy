### Deployment
 1. [Dockerize MEAN Stack Application](https://www.youtube.com/watch?v=eJgJIfWZLoM)
 2. [Create your EC2 resources and launch your EC2 instance](https://docs.aws.amazon.com/efs/latest/ug/gs-step-one-create-ec2-resources.html)
 3. SSH to EC2 Instances using Linux terminal 
    - Most Ubuntu images have a user `ubuntu`
    - Amazon's AMI is `ec2-user`
    - Most Debian images have either `root` or `admin`
    ```
    chmod 0400 ec2_key_pair.pem
    ssh -i ec2_key_pair.pem ec2-user@EC2_PUBLIC_IP
    ```
    - `ssh -i node_angular_key.pem ubuntu@108.129.8.254`
 4. Update and upgrade the system `sudo apt update && sudo apt upgrade -y`
 5. [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
 6. [Install and Use Docker Compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)
 7. Clone repo `https://gitlab.com/md-shayon/nodejs-angular-docker-aws-ec2-deploy.git`
 8. Replace .env.example with .env

- Elastic IP -> everytime we stop and start our instance it create a new public IP, therefore, we need to use elastic IP to make it static 
- elastic ip -> associate elastic IP

### Setup domain
 - route 53 -> dns management -> create hosted zone -> add domain 
 - Create record -> create a record with public ip value, cname record with www and domain name, and add name server to domain dns manager

### SSL certificate [tutorial](https://www.youtube.com/watch?v=TqgAjrs4QgQ&t=71s)
 - aws certificate manager -> request a certificate -> public certificate -> click on certificate record -> [click button]create records in route 53
 - Create a target group from load balancer -> Load balancer -> create load balancer for http and https -> Success codes (200-499) -> include as pending below -> create target group
 - Load balancer -> Application load balancer -> select https protocol -> select secutiry groups 
 - Route 53->hoosted zones -> domain -> in a record -> route traffic to "alias to application and clissic load balancer" -> select load balancer
 - If DNS is not connected to load balancer (such as godaddy, namecheap) -> copy the a record from load balancer and paste it there where dns records are managed

### Amazon CloudFront
 - 





### Host without docker
 - Connect via ssh
 - Install nodejs










 - Docker file host - domain pointing is not working
 - Install SSL certificate
core.mjs:9171 ERROR Error: Uncaught (in promise): Error: 
      auth0-spa-js must run on a secure origin. See https://github.com/auth0/auth0-spa-js/blob/master/FAQ.md#why-do-i-get-auth0-spa-js-must-run-on-a-secure-origin for more information.
    
Error: 
      auth0-spa-js must run on a secure origin. See https://github.com/auth0/auth0-spa-js/blob/master/FAQ.md#why-do-i-get-auth0-spa-js-must-run-on-a-secure-origin for more information.
    
    at auth0-spa-js.production.esm.js:1:79071
    at new e (auth0-spa-js.production.esm.js:1:78936)
    at Object.createClient [as useFactory] (auth0-auth0-angular.js:21:16)
    at Object.factory (core.mjs:8806:38)
    at R3Injector.hydrate (core.mjs:8719:35)
    at R3Injector.get (core.mjs:8607:33)
    at injectInjectorOnly (core.mjs:4782:33)
    at Module.ɵɵinject (core.mjs:4786:12)
    at Object.AuthService_Factory [as factory] (auth0-auth0-angular.js:521:1)
    at R3Injector.hydrate (core.mjs:8719:35)
    at resolvePromise (zone.js:1211:31)
    at resolvePromise (zone.js:1165:17)
    at zone.js:1278:17
    at _ZoneDelegate.invokeTask (zone.js:406:31)
    at Object.onInvokeTask (core.mjs:26261:33)
    at _ZoneDelegate.invokeTask (zone.js:405:60)
    at Zone.runTask (zone.js:178:47)
    at drainMicroTaskQueue (zone.js:585:35)








    When I try to do something fast it take more time
