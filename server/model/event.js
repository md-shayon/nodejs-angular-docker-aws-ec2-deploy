//schema of event 
const mongoose = require("mongoose"); 
const Joi = require('joi');
const { Timestamp } = require("mongodb");
const { number } = require("joi");

const addressSchema = new mongoose.Schema({
    city: {type: String, required : true},
    county: String, 
    line1: String, 
    line2: String, 
    eircode: String,
})

const eventSchema = mongoose.Schema({
    name:{
        type:String, 
        required: true
    },
    description:{
        type:String, 
        required:true
    },
    contactNumber:{
        type: String, 
        required:true
    }, 
    contact_email:{
        type:String, 
        required:true
    }, 
    // salesStartDate:{
    //     type:Date, 
    //     required:true
    // }, 
    // salesEndDate:{
    //     type:Date, 
    //     required:true
    // },
    eventDateStarts:{
        type:Date, 
        required:true
    }, 
    eventDateEnds:{
        type:Date, 
        required:true
    }, 
    
    urlImg:{
        type:String, 
        required:true
    },
   createdDate : { type: Date, default: Date.now()}, 
   
   address: addressSchema,
   startsPrice:{
    type:Number, 
    required:true
   },
   refundpolicy:{
    type:String, 
    default: "No Refounds"
   }, 
   currency:{
    type:String, 
    default:"EUR"
   },
//    tags:{
//     type:[String],
//     required: true
//  }

});


function ValidateEvent(event) {

    const addressJoiSchema = Joi.object({
        city: Joi.string()
        .min(2).required(),
        county: Joi.string(), 
        line1: 
        Joi.required(), 
        eircode: Joi.string()
        .min(3).regex(/^[ACDEFHKNPRTVWXY]{1}[0-9]{1}[0-9W]{1}[\ \-]?[0-9ACDEFHKNPRTVWXY]{4}$/
        ).required()

    })

     const eventJoiSchema = Joi.object({
        name: Joi.string()
        .min(3)
        .required()
        .messages({'string.empty': "you must enter name"}),
        contactNumber: Joi.string().min(6).regex(/^([+])?(\d+)$/).required(),
       

        // salesStartDate: Joi.date()
        // .min('now')
        // .message('"date" cannot be earlier than today')
        // .required(),

        // salesEndDate: Joi.date()
        //  .greater(Joi.ref('salesStartDate'))
        //  .max(Joi.ref('evendDateStarts'))
        //  .required(),

         eventDateStarts: Joi.date()
         .min('now')
         .message('"date" cannot be earlier than today')
         .required(),


         eventDateEnds: Joi.date()
         .required(),
 
        //  salesEndDate: Joi.date()
        //   .greater(Joi.ref('salesStartDate'))
        //   .required(),

        urlImg:Joi.string().required(),

        contact_email: Joi.string().email()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ie'] } })
        .required(),
        
        address: addressJoiSchema,

        status: Joi.string().default('Open'), 

        description: Joi.string().min(5),

        startsPrice: Joi.number(),

        // tags: Joi.array(),

     })
    
     return eventJoiSchema.validate(event);
 }
 
 const Event = mongoose.model('event', eventSchema);
 module.exports = {Event, ValidateEvent}


 


// module.exports = mongoose.model('event', eventschema); 

